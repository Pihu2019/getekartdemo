//
//  MerchantListTableViewCell.swift
//  GetePay merchant
//
//  Created by shayam on 20/07/19.
//  Copyright © 2019 eshiksa. All rights reserved.
//

import UIKit

class MerchantListTableViewCell: UITableViewCell {

    @IBOutlet weak var itemAmount: UILabel!
    @IBOutlet weak var itemInvoice: UILabel!
    @IBOutlet weak var itemDate: UILabel!
    @IBOutlet weak var itemName: UILabel!
    
    func configureMerchantCell(with merchant:MerchantModel!) {
        self.itemName.text = merchant?.itemName.capitalized
        self.itemInvoice.text = merchant?.itemSerialNo.capitalized
        self.itemDate.text = merchant?.itemExpiryDate.capitalized
        self.itemAmount.text = merchant?.itemAmount.capitalized
        
        
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
