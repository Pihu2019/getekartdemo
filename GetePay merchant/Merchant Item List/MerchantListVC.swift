

import UIKit
import Foundation

class MerchantListVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var merchantTableView: UITableView!
    
    var array = [[String:Any]]()

    var arrayMerchants = [MerchantModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.merchantTableView.delegate = self
        self.merchantTableView.dataSource = self
        
        merchantList()
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    
    
func merchantList()
{
    let headers = [
        "Content-Type": "application/json"]
    let parameters = [
        "mid": "6irdwDznArIvUbS3POLsqg==",
        "signature": "26045c0e1cdb12ca76e38fe94bb84dab8136e71626511e01b77ca000a68d0379d0e4d6a39bb3e6d492e95c4e394efd112f85f35247aef5dfffecbefc7d7de0a0",
        "itemList": [
            [
                "commissionAmount": "100",
                "commissionType": "percentage",
                "id": "",
                "itemAmount": "300",
                "itemExpiryDate": "2019-08-31",
                "modifiedDate": "2019-05-26 10:42:50",
                "itemName": "item-6",
                "itemSerialNo": "8347654321",
                "mId": "6irdwDznArIvUbS3POLsqg==",
                "totalGST": "22"
            ],
            [
                "commissionAmount": "200",
                "commissionType": "percentage",
                "id": "",
                "itemAmount": "451",
                "itemExpiryDate": "2019-08-31",
                "modifiedDate": "2019-05-24 16:42:50",
                "itemName": "item-3",
                "itemSerialNo": "8347654321",
                "mId": "6irdwDznArIvUbS3POLsqg==",
                "totalGST": "22"
            ]
        ]
        ] as [String : Any]
    
    
    APIRequest.shared.callWebServiceForJson(param: parameters as [String : AnyObject], apiTag: CTaginsertUpdateMerchantItem, successCallBack: { (resonse) in
        
        print(resonse)
        
        self.view.makeToast("Data successfully")
        
        let tempDict = resonse as! [String : Any]
        
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath:CTaginsertUpdateMerchantItem), options: .mappedIfSafe)
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
            if let jsonResult = jsonResult as? Dictionary<String, AnyObject>, let results = jsonResult["itemList"] as? [[String:Any]] {
                self.array = results
                
                let artistModelObject = MerchantModel()
                self.arrayMerchants = artistModelObject.getMerchant(list: results)
                
                debugPrint(self.arrayMerchants.count)
                // do stuff
            }
        } catch {
            // handle error
        }
        
        Datacache.insertKeyValueInUserDefault(forValue: tempDict, forKey:CUserData)
        
    }) { (error) in
        
    }
}
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayMerchants.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "MerchantListTableViewCell") as! MerchantListTableViewCell
        let merchant = self.arrayMerchants[indexPath.row]
        cell.configureMerchantCell(with: merchant)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 176
    }
}

/*
 
 {
 "status": 200,
 "mId": null,
 "message": null,
 "itemName": null,
 "signature": null,
 "itemList": [
 {
 "id": 9,
 "mId": 2,
 "itemName": "item-6",
 "itemSerialNo": "8347654321",
 "itemAmount": "300",
 "itemExpiryDate": 1567189800000,
 "createdBy": "priyagongal",
 "modifyBy": "priyagongal",
 "createDate": {
 "year": 2019,
 "month": "JULY",
 "monthValue": 7,
 "dayOfMonth": 20,
 "hour": 14,
 "minute": 52,
 "second": 12,
 "nano": 272000000,
 "dayOfWeek": "SATURDAY",
 "dayOfYear": 201,
 "chronology": {
 "id": "ISO",
 "calendarType": "iso8601"
 }
 },
 "modifyDate": {
 "year": 2019,
 "month": "JULY",
 "monthValue": 7,
 "dayOfMonth": 20,
 "hour": 14,
 "minute": 52,
 "second": 12,
 "nano": 273000000,
 "dayOfWeek": "SATURDAY",
 "dayOfYear": 201,
 "chronology": {
 "id": "ISO",
 "calendarType": "iso8601"
 }
 },
 "totalGST": "22",
 "commissionType": "percentage",
 "commissionAmount": "100",
 "itemQuantity": null,
 "hsnNo": null,
 "gstFlag": null,
 "status": true
 },
 {
 "id": 10,
 "mId": 2,
 "itemName": "item-3",
 "itemSerialNo": "8347654321",
 "itemAmount": "451",
 "itemExpiryDate": 1567189800000,
 "createdBy": "priyagongal",
 "modifyBy": "priyagongal",
 "createDate": {
 "year": 2019,
 "month": "JULY",
 "monthValue": 7,
 "dayOfMonth": 20,
 "hour": 14,
 "minute": 52,
 "second": 12,
 "nano": 288000000,
 "dayOfWeek": "SATURDAY",
 "dayOfYear": 201,
 "chronology": {
 "id": "ISO",
 "calendarType": "iso8601"
 }
 }
 */
