//
//  MerchantModel.swift
//  GetePay merchant
//
//  Created by shayam on 20/07/19.
//  Copyright © 2019 eshiksa. All rights reserved.
//

import Foundation
import UIKit

struct MerchantModel {
    var itemName = ""
    var itemAmount = ""
    var itemSerialNo = ""
    var itemExpiryDate = ""
    
    func getMerchant(list: [[String:Any]]) ->
        [MerchantModel] {
            var merchants = [MerchantModel]()
            for data in list
            {
                merchants.append(MerchantModel().getMerchant(list: data))
            }
            return merchants
    }
    
    func getMerchant(list: [String:Any]) -> MerchantModel {
        var merchant = MerchantModel()
        
        merchant.itemName = list["itemName"] as? String ?? ""
        merchant.itemAmount = list["itemAmount"] as? String ?? ""
        merchant.itemSerialNo = list["itemSerialNo"] as? String ?? ""
        merchant.itemExpiryDate = list["itemExpiryDate"] as? String ?? ""
        
        return merchant
    }
}
