////
////  VerifyOTPViewController.swift
////  GetePay merchant
////
////  Created by shayam on 10/07/19.
////  Copyright © 2019 eshiksa. All rights reserved.


import UIKit

class VerifyOTPViewController: UIViewController {
    
    @IBOutlet weak var mobileNumber: UITextField!
    @IBOutlet weak var emailId: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        
    }
    @IBAction func continueOTPBtnClicked(_ sender: Any) {
        callOTPWebService()
    }
    func callOTPWebService()
    {
//        let username = "priyagongal@gmail.com"
//        let password = "9922199406"
        let signature = "11f8bbd9dce85e344ca43e704b5fa13035c27c29c2b9195acdbd92903c8933b78a8fe320f45bf97887e0c23d55eb02e143b0e393e104ed4b31a89db19f1256ed"
        
        let param = ["email" : self.emailId.text!,
                     "mobileNo": self.mobileNumber.text,
                     "signature": signature] as! [String : Any]
        
        if appDelegate.checkConnection == 0
        {
            self.view.makeToast("Please check internet")
        }
        else
        {
            APIRequest.shared.callWebServiceForJson(param: param as [String : AnyObject], apiTag: CTagLogin, successCallBack: { (resonse) in
                
                print(resonse)
                
                self.view.makeToast("OTP sent successfully")
                
                let tempDict = resonse as! [String : Any]
                
                Datacache.insertKeyValueInUserDefault(forValue: tempDict, forKey:CUserData)
                
            }) { (error) in
                
            }
        }
        
    }
    
    /*
     {
     "email":"priyagongal@gmail.com",
     "mobileNo":"9922199406",
     "signature":"11f8bbd9dce85e344ca43e704b5fa13035c27c29c2b9195acdbd92903c8933b78a8fe320f45bf97887e0c23d55eb02e143b0e393e104ed4b31a89db19f1256ed"
     }
     
     {
     "message": "OTP updated successfully",
     "status": 303,
     "otp": "xY_5Dasb8FiSQ0e6pWz9MQ=="
     }
     
     */
    
}

//import UIKit
//import Foundation
//
//class VerifyOTPViewController: UIViewController {
//    @IBOutlet weak var mobileNum: UITextField!
//    @IBOutlet weak var email: UITextField!
//
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//
//        self.navigationController?.setNavigationBarHidden(false, animated: true)
//        self.navigationController?.navigationBar.isHidden = false
//        self.navigationController?.isNavigationBarHidden = false
//
//    }
//
//    @IBAction func continueBtnClicked(_ sender: Any) {
//
//        self .verifyOtp()
//        print(continueBtnClicked)
//    }
//
//    func verifyOtp() {
//
//    let parameters = [
//        "email": self.email.text!,
//        "mobileNo": self.mobileNum.text!,
//        "signature": "11f8bbd9dce85e344ca43e704b5fa13035c27c29c2b9195acdbd92903c8933b78a8fe320f45bf97887e0c23d55eb02e143b0e393e104ed4b31a89db19f1256ed"
//    ] as [String : Any]
//
//        let jsonData = try? JSONSerialization.data(withJSONObject: parameters)
//
//        // create post request
//        let url = URL(string: "http://35.200.153.165:8080/getekart/merchant/userGenerateOtp")!
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST"
//
//        // insert json data to the request
//        request.httpBody = jsonData
//
//        let task = URLSession.shared.dataTask(with: request) { data, response, error in
//            guard let data = data, error == nil else {
//                print(error?.localizedDescription ?? "No data")
//                return
//            }
//            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
//            if let responseJSON = responseJSON as? [String: Any] {
//                print(responseJSON)
//            }
//        }
//
//        task.resume()
//  }
//}
//
