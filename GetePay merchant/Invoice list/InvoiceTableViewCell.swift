//
//  InvoiceTableViewCell.swift
//  GetePay merchant
//
//  Created by shayam on 20/07/19.
//  Copyright © 2019 eshiksa. All rights reserved.
//

import UIKit

class InvoiceTableViewCell: UITableViewCell {

    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var invoiceNo: UILabel!
    @IBOutlet weak var coustomerName: UILabel!
    
    func configureInvoiceCell(with invoice:InvoiceModel!) {
        self.amount.text = invoice?.amount.capitalized
        
        self.invoiceNo.text = invoice?.invoiceNo.capitalized
        
        self.coustomerName.text = invoice?.customerName.capitalized
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
