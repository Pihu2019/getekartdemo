
import UIKit
import Foundation

class InvoiceListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var invoiceTableView: UITableView!
    
    var arrayInvoice = [InvoiceModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.invoiceTableView.delegate = self
        
        self.invoiceTableView.dataSource = self
        
        invoiceList()
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    
    
    func invoiceList(){
        
        
        let headers = [
            "Content-Type": "application/json",
            "cache-control": "no-cache",
            "Postman-Token": "11ece98e-6a54-4d19-b7ed-41f5c9f00a55"
        ]
        let parameters = [
            "mId": "6irdwDznArIvUbS3POLsqg==",
            "pageNo": "1",
            "signature": "27cf075b9c8a62f39b28cd3a1155b3c1a0137d10d9ff2d61f148533bf0d59edfbab4f040d986e99c955fe9322681e3093895beb5be782d3f01545689082aa7f0"
            ] as [String : Any]
        
     //   let postData = JSONSerialization.data(withJSONObject: parameters, options: [])
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://35.200.153.165:8080/getekart/merchantInvoice/getInvoiceList")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
       // request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse)
            }
        })
        
        dataTask.resume()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayInvoice.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InvoiceTableViewCell") as! InvoiceTableViewCell
        let invoice = self.arrayInvoice[indexPath.row]
        cell.configureInvoiceCell(with: invoice)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}

/*
 
 http://35.200.153.165:8080/getekart/merchantInvoice/getInvoiceList
 
 {
 "mId":"6irdwDznArIvUbS3POLsqg==",
 "pageNo":"1",
 "signature":"27cf075b9c8a62f39b28cd3a1155b3c1a0137d10d9ff2d61f148533bf0d59edfbab4f040d986e99c955fe9322681e3093895beb5be782d3f01545689082aa7f0"
 }
 
 
 {
 "id": null,
 "mId": 1,
 "invoiceVo": [
 {
 "id": 1,
 "mId": 1,
 "invoiceNo": "3333334061",
 "merchantName": "Test_Merchant",
 "merchantOrderNumber": "1345",
 "amount": 389948,
 "currency": "INR",
 "customerName": "Ram",
 "customerEmailId": "ramshankar@futuretek.in",
 "customerMobileNo": "7665221602",
 "token": "5442c0ea-17d6-4643-bcca-696ae5a826d8",
 "expireTime": 1563605464677,
 "productType": null,
 "createdDate": {
 "year": 2019,
 "month": "JULY",
 "monthValue": 7,
 "dayOfMonth": 19,
 "hour": 12,
 "minute": 21,
 "second": 4,
 "nano": 701000000,
 "dayOfWeek": "FRIDAY",
 "dayOfYear": 200,
 "chronology": {
 "id": "ISO",
 "calendarType": "iso8601"
 }
 },
 "emailStatus": true,
 "payStatus": true,
 "txnId": 7,
 "isSMS": 0,
 "isEMail": 0,
 "itemName": "[OnePlus 7 Pro, Samsung Galaxy M40, Samsung Galaxy A50, iphone-6s]",
 "itemOrderNumber": "2|2,3|5,4|4,1|3",
 "commissionAmount": "0",
 "itemGst": "6759.92",
 "totalAmount": "396707.92",
 "commissionType": "[fixed, fixed, fixed, fixed]",
 "itemSerialNo": "[45612, 45665, 7888, 256632]",
 "invoiceType": "true",
 "recursiveDate": "1",
 "couponCode": null,
 "discount": null,
 "dueCharges": null,
 "customerGstNo": null,
 "invoiceNotes": "testing invoice note-1,testing invoice note-2,testing invoice note-3,testing invoice note-4,testing invoice note-5",
 "invoiceDescription": "this is testing invoice",
 "expired": true
 },
 {
 "id": 2,
 "mId": 1,
 "invoiceNo": "3333376044",
 "merchantName": "Test_Merchant",
 "merchantOrderNumber": "13456",
 "amount": 1200,
 "currency": null,
 "customerName": "Ram",
 "customerEmailId": "ramshankar@futuretek.in",
 "customerMobileNo": "7665221602",
 "token": "dde5da04-82f5-488f-8ad9-6c267ceba93c",
 "expireTime": 1563605595515,
 "productType": null,
 "createdDate": {
 "year": 2019,
 "month": "JULY",
 "monthValue": 7,
 "dayOfMonth": 19,
 "hour": 12,
 "minute": 23,
 "second": 15,
 "nano": 517000000,
 "dayOfWeek": "FRIDAY",
 "dayOfYear": 200,
 "chronology": {
 "id": "ISO",
 "calendarType": "iso8601"
 }
 }
*/
