//
//  Invoice.swift
//  GetePay merchant
//
//  Created by shayam on 20/07/19.
//  Copyright © 2019 eshiksa. All rights reserved.
//

import Foundation
import UIKit

struct InvoiceModel {
    var invoiceNo = ""
    var customerName = ""
    var amount = ""
    
    
    func  getInvoice(list:[[String:Any]]) -> [InvoiceModel] {
        var invoices = [InvoiceModel]()
        for data in list{
            invoices.append(InvoiceModel().getInvoice(list: data))
        }
        return invoices
    }
    
    func getInvoice(list:[String:Any]) -> InvoiceModel {
        var invoice = InvoiceModel()
        invoice.invoiceNo = list["invoiceNo"] as? String ?? ""
        invoice.customerName = list["customerName"] as? String ?? ""
        invoice.amount = list["amount"] as? String ?? ""
        
        return invoice
    }
}
