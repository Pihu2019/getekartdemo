//
//  ApplicationConstants.swift
//  Food Ordering App
//
//
//test
import Foundation
import UIKit
//MARK:-
//MARK:- Fonts



//  Converted to Swift 5 by Swiftify v5.0.29819 - https://objectivec2swift.com/
let TopHeight = (UIWindow(frame: UIScreen.main.bounds).safeAreaInsets.top) != 0.0 ? (UIWindow(frame: UIScreen.main.bounds).safeAreaInsets.top) : 20.0
let BootomHeight = Float(UIDevice.current.systemVersion) ?? 0.0 >= 11.0 ? (UIWindow(frame: UIScreen.main.bounds).safeAreaInsets.bottom) : 0.0
//  Converted to Swift 5 by Swiftify v5.0.29819 - https://objectivec2swift.com/
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
let SCREEN_WIDTH = UIScreen.main.bounds.size.width
//let PORTRAIT_KEYBOARD_HEIGHT = 216



let CTabBarText = "text"
let CTabBarIcon = "image"
let CTabBarWidth = "width"



let CMainStoryboard = UIStoryboard(name: "Main", bundle: nil)
let COrderStoryboard = UIStoryboard(name: "Order", bundle: nil)

//MARK:-
//MARK:- Color


let CColorWhite  = CRGBA(r: 255.0, g: 255.0, b: 255.0, a: 1.0)
let CColorBlack  =  CRGB(r: 0, g: 0, b: 0)
let CColorGreenMedicalwale  =  CRGB(r: 4, g: 147, b: 65)
let CColorGreenShadow  = CRGBA(r: 0, g: 223, b: 141, a: 0.3)
let CColorRedMedicalwale      =    CRGB(r: 254, g: 31, b: 32)
let CColorLightGrey  =   CRGB(r: 205, g: 206, b: 207)
let CColorDarkGrey     =      CRGB(r: 95, g: 96, b: 97)
let CColorSilver     =      CRGB(r: 228, g: 232, b: 233)
let CColorBlueMedicalwale     =      CRGB(r: 0, g: 103, b: 222)
let CColorBlueText      = CRGB(r: 0, g: 85, b: 255)

let CProductSearch = UIStoryboard(name: "ProductSearch", bundle: nil)
let CMain = UIStoryboard(name: "Main", bundle: nil)
let CMainDash = UIStoryboard(name: "DashBoard", bundle: nil)
let CFilter = UIStoryboard(name: "Filter", bundle: nil)
let CUserData           = "userData"
let CJsonResponse       = "response"
let CJsonError          = "error"
let CJsonErrorFalse     = "false"

let CJsonMessage        = "message"
let CJsonStatus         = "status"
let CStatusCode         = "status_code"
let CJsonTitle          = "title"
let CJsonData           = "data"
let CJsonCode           = "code"
let CJsonMeta           = "meta"

let CGini        = "gini"
let CUser       = "user"
//let CStatusCode         = "status_code"
//let CJsonTitle          = "title"
//let CJsonData           = "data"
//let CJsonCode           = "code"
//let CJsonMeta           = "meta"


let MSGTextType = "text"
let MSGListType = "venderList"
let MSGDetailsType = "venderDetails"
let MSGTimeSlot = "time_slot"
let MSGFamily = "User Relation Listing"
let MSGPaymentDoctor = "PaymentDoctor"
let MSGAppointmentList = "AppoinmentListing"
let MSGLoading = "loading"


let kCancel = "Cancelled"
let kConfirm = "Confirmed"
//let kCARTORDERCancelord = "Order Cancelled"
let kAwaitingConfirmation = "Awaiting Confirmation"
//let kCARTORDERAwaiting = "Awaiting Confirmation"
let kComplete = "Completed"
let kPayComplete = "Payment Completed"
let KCap = "Cash At Point"


let CStatusZero         = NSNumber(value: 0 as Int)
let CStatusOne          = NSNumber(value: 1 as Int)
let CStatusTwo          = NSNumber(value: 2 as Int)
let CStatusThree        = NSNumber(value: 3 as Int)
let CStatusFour         = NSNumber(value: 4 as Int)
let CStatusFive         = NSNumber(value: 5 as Int)
let CStatusNine         = NSNumber(value: 9 as Int)
let CStatusEight        = NSNumber(value: 8 as Int)
let CStatusTen          = NSNumber(value: 10 as Int)

let CStatusTwoHundred   = NSNumber(value: 200 as Int)       //  Success
let CStatusFourHundredAndOne = NSNumber(value: 401 as Int)     //  Unauthorized user access
let CStatusFiveHundredAndFiftyFive = NSNumber(value: 555 as Int)   //  Invalid request
let CStatusFiveHundredAndFiftySix = NSNumber(value: 556 as Int)   //  Invalid request
let CStatusFiveHundredAndFifty = NSNumber(value: 550 as Int)        //  Inactive/Delete user



let appDelegate = UIApplication.shared.delegate as! AppDelegate
let CUserDefaultBaseUrl    =   "CUserDefaultBaseUrl"

//MARK:-
//MARK:- Localization
func LocalizedString(key: String, comment: String) -> Any
{
    return Localization.sharedInstance.localizedString(forKey: key , value: comment)
}

func CLocalize(text: String) -> String {
    return Localization.sharedInstance.localizedString(forKey: text , value: text)
}

