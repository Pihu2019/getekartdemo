//  Localization.swift
//  CollectMee
//  Created by mac-0007 on 13/12/17.
//  Copyright © 2017 mac-0007. All rights reserved
//  Copyright 

import Foundation

func LocalizationSetLanguage(language: String)
{
    Localization.sharedInstance.setLanguage(language)
}

let LocalizationGetLanguage     = Localization.sharedInstance.getLanguage()
//let LocalizationReset         = Localization.sharedInstance.resetLocalization()

class Localization: NSObject
{
    static let sharedInstance = Localization()
    private override init() {}           //This prevents others from using the default '()' initializer for this class.
    static var bundle  = Bundle.main
    
    
    // Example calls:
    // AMLocalizedString(@"Text to localize",@"Alternative text, in case the other is not find");
    
    func localizedString(forKey key: String, value comment: String) -> String
    {
        return Localization.bundle.localizedString(forKey: key, value: comment, table: nil)
    }
    
    // Sets the desired language of the ones you have.
    // example calls:
    // LocalizationSetLanguage(@"Italian");
    // LocalizationSetLanguage(@"German");
    // LocalizationSetLanguage(@"Spanish");
    
    
    // If this function is not called it will use the default OS language.
    // If the language does not exists y returns the default OS language.
    
    func setLanguage(_ l: String) {
        print("preferredLang: \(l)")
        let path: String? = Bundle.main.path(forResource: l, ofType: "lproj")
        if path == nil {
            //in case the language does not exists
            resetLocalization()
        }
        else {
            Localization.bundle = Bundle(path: path ?? "")!
        }
    }
    
    // Just gets the current setted up language.
    // returns "es","fr",...
    //
    // example call:
    // NSString * currentL = LocalizationGetLanguage;
    
    func getLanguage() -> String {
        let languages = UserDefaults.standard.object(forKey: "AppleLanguages") as? [Any]
        let preferredLang = languages?[0] as? String
        return preferredLang ?? ""
    }
    
    // Resets the localization system, so it uses the OS default language.
    //
    // example call:
    // LocalizationReset;
    
    func resetLocalization() {
        Localization.bundle = Bundle.main
    }
}


