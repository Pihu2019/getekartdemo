//
//  Datacache.swift
//  Gni
//
//  Created by AEGIS Health Solutions  Pvt. Ltd. on 02/04/19.
//  Copyright © 2019 AEGIS Health Solutions  Pvt. Ltd. All rights reserved.
//

import UIKit

class Datacache: NSObject {

    class func getValueFromUserDefault(forKey key: String?) -> [String : Any]? {
        var valueabc: [String : Any]
        
          valueabc = [:]
        let applicationUserDefault = UserDefaults.standard
        
        let data = applicationUserDefault.object(forKey: key ?? "") as? Data
        if let data = data, let unarchive = NSKeyedUnarchiver.unarchiveObject(with: data) as? [String : Any] {
            valueabc = unarchive
        }
        // value = [dict valueForKey:key];
        
        
        applicationUserDefault.synchronize()
        return valueabc
    }
    
    class func removeKeyValueFromUserDefault(forKey key: String?) {
        let applicationUserDefault = UserDefaults.standard
        applicationUserDefault.set(nil, forKey: key ?? "")
        applicationUserDefault.synchronize()
    }
    
    class func insertKeyValueInUserDefault(forValue value: [String : Any]?, forKey key: String?) {
        
        var dataSave: Data? = nil
        if let value = value {
            dataSave = NSKeyedArchiver.archivedData(withRootObject: value)
        }
        let applicationUserDefault = UserDefaults.standard
        applicationUserDefault.set(dataSave, forKey: key ?? "")
        applicationUserDefault.synchronize()
    }
    
}
