//
//  Utility.swift
//  Gni
//
//  Created by AEGIS Health Solutions  Pvt. Ltd. on 02/04/19.
//  Copyright © 2019 AEGIS Health Solutions  Pvt. Ltd. All rights reserved.
//

import UIKit
private let KEYBOARD_ANIMATION_DURATION: CGFloat = 0.3
private let MINIMUM_SCROLL_FRACTION: CGFloat = 0.2
private let MAXIMUM_SCROLL_FRACTION: CGFloat = 0.8
private let PORTRAIT_KEYBOARD_HEIGHT: CGFloat = 264
private let LANDSCAPE_KEYBOARD_HEIGHT: CGFloat = 352
private var animatedDistance: CGFloat = 0.0

class Utility: NSObject {

     // MARK:- ---> Chat Utility
    
    class func findHeightOfCell(forText text: String? , selected:Int) -> CGFloat {
    
        var returnHeight = 0
        if text == MSGLoading
        {
            returnHeight = 40
        }
        if text == MSGListType
        {
            returnHeight = 190
        }
        if text == MSGDetailsType
        {
            if selected == 0
            {
                 returnHeight = 445
            }
            else
            {
                 returnHeight = 290
            }
        }
        if text == MSGTimeSlot
        {
            if selected == 0
            {
               returnHeight = 125
            }
            else
            {
                returnHeight = 125
            }
        }
        if text == MSGFamily
        {
            if selected == 0
            {
                returnHeight = 15
            }
            else
            {
                returnHeight = 15
                
            }
        }
        if text == MSGPaymentDoctor
        {
            if selected == 0
            {
                returnHeight = 450
            }
            else
            {
                returnHeight = 260
                
            }
        }
        if text == MSGAppointmentList
        {
            if selected == 0
            {
                returnHeight = 30
            }
            else
            {
                returnHeight = 30
                
            }
        }
        return CGFloat(returnHeight)
        }
    
    class func getruppesvaluefromnumber(_ number: String?) -> String? {
        let indCurrencyFormatter = NumberFormatter()
        indCurrencyFormatter.numberStyle = .currency
        indCurrencyFormatter.locale = NSLocale(localeIdentifier: "en_IN") as Locale
        let formattedString = indCurrencyFormatter.string(from: NSNumber(value: Float(number ?? "") ?? 0.0))
        return formattedString
    }
    
    class func getruppesvaluefromwithoutSymbolnumber(_ number: String?) -> String? {
        let nf = NumberFormatter()
        nf.numberStyle = .currency
        nf.currencySymbol = "" // <-- this
        let number2 = NSDecimalNumber(string: number)
        let price = nf.string(from: number2)
        return price
    }
     // MARK:- ---> TextFeild Utility
    class func addPaddingView(to textField: UITextField?, length: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: length, height: 34))
        textField?.leftView = paddingView
        textField?.leftViewMode = .always
    }
    
    class func addPaddingView(toTextFieldtop textField: UITextView?, length: CGFloat) {
        
        textField?.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        textField?.layer.cornerRadius = 6
    }
    
   
    class func moveTextFieldUp(for forView: UIView?, for textField: UITextField?, forSubView: UIView?) {
        let textFieldRect = forView?.window?.convert(textField?.bounds ?? CGRect.zero, from: textField)
        let viewRect = forView?.window?.convert(forSubView?.bounds ?? CGRect.zero, from: forSubView)
        
        let midline: CGFloat = (textFieldRect?.origin.y ?? 0.0) + 0.5 * (textFieldRect?.size.height ?? 0.0)
        let numerator: CGFloat = midline - (viewRect?.origin.y ?? 0.0) - MINIMUM_SCROLL_FRACTION * (viewRect?.size.height ?? 0.0)
        let denominator: CGFloat = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * (viewRect?.size.height ?? 0.0)
        var heightFraction: CGFloat = numerator / denominator
        
        if heightFraction < 0.0 {
            heightFraction = 0.0
        } else if heightFraction > 1.0 {
            heightFraction = 1.0
        }
        
        let orientation: UIInterfaceOrientation = UIApplication.shared.statusBarOrientation
        if orientation == .portrait || orientation == .portraitUpsideDown {
            animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction)
        } else {
            animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction)
        }
        var viewFrame: CGRect? = forSubView?.frame
        viewFrame?.origin.y -= animatedDistance
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(TimeInterval(KEYBOARD_ANIMATION_DURATION))
        
        forSubView?.frame = viewFrame ?? CGRect.zero
        
        UIView.commitAnimations()
    }
    
    class func moveTextFieldDownforView(_ forSubView: UIView?) {
        var viewFrame: CGRect? = forSubView?.frame
        viewFrame?.origin.y += animatedDistance
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(TimeInterval(KEYBOARD_ANIMATION_DURATION))
        
        forSubView?.frame = viewFrame ?? CGRect.zero
        
        UIView.commitAnimations()
    }

    //  Converted to Swift 5 by Swiftify v5.0.29819 - https://objectivec2swift.com/
   
    class func findWidth(forText text: String?, havingWidth widthValue: CGFloat, andFont font: UIFont?) -> CGFloat {
        var size = CGSize.zero
        if text != nil {
            var frame: CGRect? = nil
            if let font = font {
                frame = text?.boundingRect(with: CGSize(width: widthValue, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: [
                    NSAttributedString.Key.font: font
                    ], context: nil)
            }
            size = CGSize(width: frame?.size.width ?? 0.0, height: (frame?.size.height ?? 0.0) + 1)
        }
        return size.width
    }
    
    //Dynamic cell height
    class func findHeight(forText text: String?, havingWidth widthValue: CGFloat, andFont font: UIFont?) -> CGFloat {
        var size = CGSize.zero
        if text != nil {
            var frame: CGRect? = nil
            if let font = font {
                frame = text?.boundingRect(with: CGSize(width: widthValue, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: [
                    NSAttributedString.Key.font: font
                    ], context: nil)
            }
            size = CGSize(width: frame?.size.width ?? 0.0, height: (frame?.size.height ?? 0.0) + 1)
        }
        return size.height
    }

    class func roundedimgView(_ viewround: UIImageView?, color: UIColor?) {
        viewround?.layer.cornerRadius = (viewround?.frame.size.height)!/2
        viewround?.clipsToBounds = true
         viewround?.layer.borderWidth = 1.0
          viewround?.layer.borderColor = color?.cgColor
        
        //    imageViewObj.layer.cornerRadius=imageViewObj.frame.size.height/2;
        //    [imageViewObj setClipsToBounds:YES];
    }
    
    class func roundedUIViewOfColor(_ viewround: UIView?, color: UIColor?) {
       
        viewround?.layer.cornerRadius = (viewround?.frame.size.height)!/2
        viewround?.layer.borderWidth = 1.0
          viewround?.clipsToBounds = true
        viewround?.layer.borderColor = color?.cgColor
        //    imageViewObj.layer.cornerRadius=imageViewObj.frame.size.height/2;
        //    [imageViewObj setClipsToBounds:YES];
    }
    class func squareUIViewOfColor(_ viewround: UIView?, color: UIColor?) {
        
        viewround?.layer.cornerRadius = 5
        viewround?.layer.borderWidth = 1.0
        viewround?.clipsToBounds = true
        viewround?.layer.borderColor = color?.cgColor
        //    imageViewObj.layer.cornerRadius=imageViewObj.frame.size.height/2;
        //    [imageViewObj setClipsToBounds:YES];
    }
    
    // MARK:- ---> Date and time
    //  Converted to Swift 4 by Swiftify v4.2.23619 - https://objectivec2swift.com/
    class func string(toFormatedDateShowOnscreen date: Date?) -> String? {
        // NSString *finalDate = @"2014-10-15";
        
        let dateFormatter = DateFormatter()
        // [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        // NSDate *date = [Utility dateToFormatedDateString:dateStr];
        dateFormatter.dateFormat = "dd MMM, YYYY"
        if let date = date {
            return dateFormatter.string(from: date)
        }
        return nil
    }
    class func date(inUserFormatDateWithTime dateStr: String?) -> String? {
        // NSString *finalDate = @"2014-10-15";
        
        let dateFormatter = DateFormatter()
        // [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        let date: Date? = Utility.date(toFormatedDateString: dateStr)
        dateFormatter.dateFormat = "EEE, YYYY MMM dd hh:mma"
        if let date = date {
            return dateFormatter.string(from: date)
        }
        return nil
    }
    
    class func dateToDate(inUserFormatDateWithTime dateStr: Date?) -> String? {
        // NSString *finalDate = @"2014-10-15";
        
        let dateFormatter = DateFormatter()
        // [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        // let date: Date? = Datacache.date(toFormatedDateString: dateStr)
        dateFormatter.dateFormat = "EEE, YYYY MMM dd hh:mma"
        if let date = dateStr {
            return dateFormatter.string(from: date)
        }
        return nil
    }
    class func dateTodate(inUserFormatDate dateStr: Date?) -> String? {
        // NSString *finalDate = @"2014-10-15";
        
        let dateFormatter = DateFormatter()
        // [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        // let date: Date? = Datacache.date(toFormatedDateString: dateStr)
        dateFormatter.dateFormat = "EEE, dd MMM YYYY"
        if let date = dateStr {
            return dateFormatter.string(from: date)
        }
        return nil
    }
    class func dateToDate(inUserFormatDate dateStr: Date?) -> String? {
        // NSString *finalDate = @"2014-10-15";
        
        let dateFormatter = DateFormatter()
        // [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        // let date: Date? = Datacache.date(toFormatedDateString: dateStr)
        dateFormatter.dateFormat =  "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let date = dateStr {
            return dateFormatter.string(from: date)
        }
        return nil
    }
    class func dateToFormatedForDay(inUserFormatDate dateStr: String?) -> String? {
        // NSString *finalDate = @"2014-10-15";
        
        let dateFormatter = DateFormatter()
        // [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        let date: Date? = Utility.date(toFormatedDateString: dateStr)
        dateFormatter.dateFormat = "dd MMM"
        if let date = date {
            return dateFormatter.string(from: date)
        }
        return nil
    }
    class func dateToFormatedForWeek(inUserFormatDate dateStr: String?) -> String? {
        // NSString *finalDate = @"2014-10-15";
        
        let dateFormatter = DateFormatter()
        // [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        let date: Date? = Utility.date(toFormatedDateString: dateStr)
        dateFormatter.dateFormat = "EEE"
        if let date = date {
            return dateFormatter.string(from: date)
        }
        return nil
    }
    class func dateToFormatedForHoursAndmininute(inUserFormatDate dateStr: String?) -> String? {
        // NSString *finalDate = @"2014-10-15";
        
        let dateFormatter = DateFormatter()
        // [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        let date: Date? = Utility.date(toFormatedDateString: dateStr)
        dateFormatter.dateFormat = "hh:mm a"
        if let date = date {
            return dateFormatter.string(from: date)
        }
        return nil
    }
    class func date(toFormatedDateString dateStr: String?) -> Date? {
        
        var detectedDate: Date?
        
        //Detect.
        if dateStr! as NSString != NSNull() {
            
            
            let detector = try? NSDataDetector(types: NSTextCheckingAllTypes)
            detector?.enumerateMatches(in: dateStr ?? "", options: [], range: NSRange(location: 0, length: dateStr?.count ?? 0), using: { result, flags, stop in
                
                detectedDate = result?.date
                
            })
        }
        
        return detectedDate
    }
}
