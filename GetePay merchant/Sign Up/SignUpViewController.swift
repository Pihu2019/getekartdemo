//
//  SignUpViewController.swift
//  GetePay merchant
//
//  Created by shayam on 20/07/19.
//  Copyright © 2019 eshiksa. All rights reserved.
//

import UIKit
import Foundation

class SignUpViewController: UIViewController {
    @IBOutlet weak var emailId: UITextField!
    @IBOutlet weak var signUpView: UIView!
    @IBOutlet weak var city: UITextField!
    
    @IBOutlet weak var categoryId: UITextField!
    @IBOutlet weak var partnerId: UITextField!
    @IBOutlet weak var pincode: UITextField!
    @IBOutlet weak var country: UITextField!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var mobileNumber: UITextField!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var merchantName: UITextField!
    
    
    @IBOutlet weak var loginBtn: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func loginBtnClicked(_ sender: Any) {
    }
    func signUp(){
        
        
        let headers = [
            "Content-Type": "application/json",
            "cache-control": "no-cache",
            "Postman-Token": "77b5544e-77ea-47c2-9200-7db8a3912f2e"
        ]
        let parameters = [
            "otp": "Fikq01IKXiD1D+h_D4LPsQ==",
            "merchantName": "Shantanu",
            "mobileNumber": "9922199406",
            "email": "shantanu@gmail.com",
            "username": "priyagongal",
            "password": "Admin@123",
            "address": "Second floor, Behind St.Joseph",
            "country": "India",
            "city": "Lucknow",
            "pincode": 273007,
            "channelPartnerId": 2,
            "categoryId": 1,
            "signature": "bb6ba12744497d4d43eb5e2ac794aa5266c9ecec4912cdd00cf56440b470c93b78dd7675000e9fc5c0be4d6e8e3b5ea981ba8ea3184cfb601b77fd4101fe5a60"
            ] as [String : Any]
        
      //  let postData = JSONSerialization.data(withJSONObject: parameters, options: [])
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://35.200.153.165:8080/getekart/merchant/saveMerchants")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
      //  request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse)
            }
        })
        
        dataTask.resume()
    }

}

/*
 
 
 http://35.200.153.165:8080/getekart/merchant/saveMerchants

 
 
 {
 "otp": "Fikq01IKXiD1D+h_D4LPsQ==",
 "merchantName":"Shantanu",
 "mobileNumber":"9922199406",
 "email" : "shantanu@gmail.com",
 "username":"priyagongal",
 "password":"Admin@123",
 "address":"Second floor, Behind St.Joseph",
 "country":"India",
 "city":"Lucknow",
 "pincode":273007,
 "channelPartnerId":2,
 "categoryId":1,
 "signature":"bb6ba12744497d4d43eb5e2ac794aa5266c9ecec4912cdd00cf56440b470c93b78dd7675000e9fc5c0be4d6e8e3b5ea981ba8ea3184cfb601b77fd4101fe5a60"
 }

 
 
 {
 "status": 200,
 "message": "Merchant updated sucessfully !!",
 "mid": "IYMJuIFG9d4lDP+PBC1I2A=="
 }
 */
 
