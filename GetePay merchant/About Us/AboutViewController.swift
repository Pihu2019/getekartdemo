//
//  AboutViewController.swift
//  GetePay merchant
//
//  Created by shayam on 20/07/19.
//  Copyright © 2019 eshiksa. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


/*
 
 http://35.200.153.165:8080/getekart/header/getAboutUsDetail
 
 
 {
 "mid" : "6irdwDznArIvUbS3POLsqg==",
 "type":"Term and Condition",
 "signature" : "eef98f1f7ae748f55669f0d582f315efbdb105c85f5665c0110335976f83f2a4580b44c3738f87bceeb08e0d73b21958aed12575ce7f3c26573b7bacb762ae0f"
 }
 
 
 {
 "mId": 1,
 "message": "Success",
 "status": 200,
 "data": "<p>Technology is going mainstream in India. Everyone we know is using technology to get smarter at how they live, work and consume entertainment.&nbsp;In India, technology is disrupting legacy business models and creating software-enabled sectors that have never existed before. Yesterday&rsquo;s disrupters in the sectors of finance, retail, healthcare and services are today&rsquo;s incumbents.</p>\r\n<p>Amid all this, Indians are looking for insights that will help them make sense of the way our world is changing because of technology, and also empower them with ideas to compete in this new world. It will help its audience cut through the chaff and make sense of the future.</p>\r\n<p>For technology consumers,It will be the destination to discover life-changing products and ideas. Not just the cheapest phones, but gadgets and ideas that can truly transform their lives.&nbsp;For those working in the business sectors,It will present realistic and in-depth commentary and stories about the companies and people that matter.&nbsp;We&nbsp;will ask hard-hitting questions and relentlessly hunt for the answers.</p>\r\n<p>Most importantly,It sits at the intersection of technology with life, culture and society in India. We are bringing together the streams of storytelling, journalism, technology and design to build a smart newsroom.&nbsp;At the core, we believe in the values of traditional journalism and are absolutely religious about ethics and integrity.</p>"
 }
 
 */
