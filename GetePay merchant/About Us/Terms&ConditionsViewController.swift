//
//  Terms&ConditionsViewController.swift
//  GetePay merchant
//
//  Created by shayam on 20/07/19.
//  Copyright © 2019 eshiksa. All rights reserved.
//

import UIKit

class Terms_ConditionsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


/*
 http://35.200.153.165:8080/getekart/header/getTermsAndCondition

 
 {
 "mid" : "6irdwDznArIvUbS3POLsqg==",
 "type":"Term and Condition",
 "signature" : "eef98f1f7ae748f55669f0d582f315efbdb105c85f5665c0110335976f83f2a4580b44c3738f87bceeb08e0d73b21958aed12575ce7f3c26573b7bacb762ae0f"
 }

 
 
 {
 "mId": 1,
 "message": "Success",
 "status": 200,
 "data": "<h2>Influence of choices</h2>\r\n<p>Every choice that we make is influenced by someone who has made a conscious decision to communicate the options in a certain manner. Think about it.</p>\r\n<ol>\r\n<li>A supermarket displaying certain products at the eye level.</li>\r\n<li>A shop keeper pushing features of a certain mobile phone.</li>\r\n<li>A food ordering app displaying the bestsellers on top of the screen.</li>\r\n<li>An e-commerce store highlighting discounts on some items.</li>\r\n</ol>\r\n<p>The individual who influences your decision in the above context is called a choice architect.</p>\r\n<hr class=\"wp-block-separator\" />\r\n<h2>Choice architect</h2>\r\n<p>A choice architect has the responsibility for organising the environment in which people make decisions. In short, all designers are choice architects.</p>\r\n<h4>The responsibility of a choice architect</h4>\r\n<p>Every choice architect has an immense responsibility because they influence decisions made by others. They are expected to create those choices that are in the interest of the end user. Every insignificant detail can have major impact on people&rsquo;s behaviour. A good rule to be able to do this job well is to assume that &lsquo;everything matters&rsquo;.</p>\r\n<hr class=\"wp-block-separator\" />\r\n<p>There are two schools of thoughts on how choice architects think about communicating choices to people.</p>"
 }
 
 */
