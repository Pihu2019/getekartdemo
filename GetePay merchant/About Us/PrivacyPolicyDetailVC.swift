//
//  PrivacyPolicyDetailVC.swift
//  GetePay merchant
//
//  Created by shayam on 20/07/19.
//  Copyright © 2019 eshiksa. All rights reserved.
//

import UIKit

class PrivacyPolicyDetailVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

/*
 
 URL :  http://35.200.153.165:8080/getekart/header/getPrivacyPolicyDetail
 
 
 Request:-
 
 {
 "mid" : "6irdwDznArIvUbS3POLsqg==",
 "type":"Privacy Policy",
 "signature" : "9b49fc8e341f16e82f050e66400843e816be95ae966480ce90584375c04ec9c364c0a644bb63b145bf3e815f4e8d94b76e2f6046195026d8b1d834d5941d4dea"
 }
 
 
 
 Response:-
 
 {
 "mId": 1,
 "message": "Success",
 "status": 200,
 "data": "This is test detail"
 }

 
 
 */
