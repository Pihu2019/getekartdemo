//
//  FeedbackViewController.swift
//  GetePay merchant
//
//  Created by shayam on 20/07/19.
//  Copyright © 2019 eshiksa. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

/*
 
 http://35.200.153.165:8080/getekart/header/saveFeedBackDetail

 {
 "mid" : "6irdwDznArIvUbS3POLsqg==",
 "firstName" : "Abhishek",
 "lastName":"Ojha",
 "email" : "ojha92@gmail.com",
 "mobileNo":"9513478369",
 "remark":"This is test remark",
 "signature" : "a06e334310f7cf18202b55c84d3fa55d71567a4ec66a0862a79406dea3f13b15e14c594a9710fe1e8eff8186f7b889990a2c96209f0de1373bf6088cf544c002"
 }

 
 {
 "mId": 1,
 "feedbackList": null,
 "message": "Successfully send your feadback !!",
 "status": 200
 }
 
 */
