//
//  ContactUsViewController.swift
//  GetePay merchant
//
//  Created by shayam on 20/07/19.
//  Copyright © 2019 eshiksa. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

/*
 
 http://35.200.153.165:8080/getekart/header/getContactUsDetail
 
 
 Request:-
 
 {
 "mid" : "6irdwDznArIvUbS3POLsqg==",
 "type":"Contact Us",
 "signature" : "b06e63237c3ea1ce9a67207af253cb143dd0e5382697868cecb1339b6ce617f516f7bf4a3faabba65d64034e641e41244eba775b52987c7ed718bc5b316a191f"
 }
 
 
 
 Response:-
 
 {
 "mId": 1,
 "message": "Success",
 "status": 200,
 "data": "This is test2222"
 }
 

 */
