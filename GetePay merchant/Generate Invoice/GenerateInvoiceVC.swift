//
//  GenerateInvoiceVC.swift
//  GetePay merchant
//
//  Created by shayam on 20/07/19.
//  Copyright © 2019 eshiksa. All rights reserved.
//

import UIKit
import Foundation

class GenerateInvoiceVC: UIViewController {

    @IBOutlet weak var invoiceView: UIView!
    @IBOutlet weak var coustomerEmail: UITextField!
    @IBOutlet weak var coustomerAmount: UITextField!
    @IBOutlet weak var coustomerMobileNo: UITextField!
    @IBOutlet weak var coustomerName: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        invoiceView.layer.cornerRadius = 10
        submitBtn.layer.cornerRadius = 20
    }
    

    @IBAction func submitButtonClicked(_ sender: Any) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func generateInvoice(){
        
        
        let headers = [
            "Content-Type": "application/json",
            "cache-control": "no-cache",
            "Postman-Token": "11ece98e-6a54-4d19-b7ed-41f5c9f00a55"
        ]
        let parameters = [
            "mId": "6irdwDznArIvUbS3POLsqg==",
            "pageNo": "1",
            "signature": "27cf075b9c8a62f39b28cd3a1155b3c1a0137d10d9ff2d61f148533bf0d59edfbab4f040d986e99c955fe9322681e3093895beb5be782d3f01545689082aa7f0"
            ] as [String : Any]
        
        //let postData = JSONSerialization.data(withJSONObject: parameters, options: [])
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://35.200.153.165:8080/getekart/merchantInvoice/getInvoiceList")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        //request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse)
            }
        })
        
        dataTask.resume()
    }

}

/*
 
 
 http://35.200.153.165:8080/getekart/itemInvoice/generateInvoice
 
 
 {
 "customerEmailId" : "ramshankar@futuretek.in",
 "customerMobileNo" : "7665221602",
 "customerName" : "ram tyagi",
 "emailStatus" :true,
 "id" : "1 2",
 "itemName" : "item-1|1 item-2|2",
 "merchantOrderNo" : "000023431991",
 "mId" : "6irdwDznArIvUbS3POLsqg==",
 "noOfItems" : "2|1 4|2",
 "recursive" : true,
 "recursiveDate" : "25",
 "customerId" : "23",
 "signature" : "91625fcb9a24a71c60d4b6bdc8ec5fcd7b2cd3c29e1b1b18556ce8d586b539ebd9dc3df5117200c34baae1c451267b63acad53d7e787ea479805369ed6070588"
 }
 
 
 {
 "status": 200,
 "mId": 1,
 "message": "payment request send for this ramshankar@futuretek.in",
 "itemName": null,
 "signature": "91625fcb9a24a71c60d4b6bdc8ec5fcd7b2cd3c29e1b1b18556ce8d586b539ebd9dc3df5117200c34baae1c451267b63acad53d7e787ea479805369ed6070588",
 "itemList": null,
 "customerId": 2
 }

 
 */
 
