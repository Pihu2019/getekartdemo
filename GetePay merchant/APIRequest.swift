
import Foundation
import UIKit
import Alamofire

//MARK:-
//MARK:- Networking Class

/// API Tags

let CTagLogin                       = "index.php?plugin=MobileAPI&action=getRequest"
let CTaginsertUpdateMerchantItem  = "http://35.200.153.165:8080/getekart/itemInvoice/insertUpdateMerchantItem"

class Networking
{
    typealias ClosureSuccess = (_ task:URLSessionTask, _ response:AnyObject?) -> Void
    typealias ClosureError   = (_ task:URLSessionTask, _ error:NSError?) -> Void
    
    var BASEURL:String = "http://demo.colonyworld.com/colonyworld/colony/"
    // var GniBASEURL:String = "http://139.59.76.89:8002/gni/"
     var GniBASEURL:String = "http://34.207.160.225:8002/"
    
   
//    var headers:[String: String]?
   
    
    var headers:[String: String] {

//        let headersData = [
//            "Auth-Key": "medicalwalerestapi",
//            "Authorizations": "25iwFyq/LSO1U",
//            "Client-Service": "frontend-client",
//            "Content-Type": "application/json",
//            "User-ID": "1"
//        ]
        
        
        let headersData = [
            
            "Content-Type": "application/json"
           
        ]

        
        return headersData
    }
    
    var loggingEnabled = true
    var activityCount = 0
    //    var strToken = "Bearer: " + appDelegate.loginUser.token!
    
    /// Networking Singleton
    static let sharedInstance = Networking()
    
    private init() {
//        BASEURL = APIRequest.shared.BASEURL
    }
    
    fileprivate func logging(request req:Request?) -> Void
    {
        if (loggingEnabled && req != nil)
        {
            var body:String = ""
            var length = 0
            
            if (req?.request?.httpBody != nil) {
                body = String.init(data: (req!.request!.httpBody)!, encoding: String.Encoding.utf8)!
                length = req!.request!.httpBody!.count
            }
            
            if (req?.request != nil)
            {
                let printableString = "\(req!.request!.httpMethod!) '\(req!.request!.url!.absoluteString)': \(String(describing: req!.request!.allHTTPHeaderFields)) \(body) [\(length) bytes]"
                
                print("API Request: \(printableString)")
            }
            
        }
    }
    
    fileprivate func logging(response res:DataResponse<Any>) -> Void
    {
        if (loggingEnabled)
        {
            if (res.result.error != nil) {
                print("API Response: (\(String(describing: res.response?.statusCode))) [\(res.timeline.totalDuration)s] Error:\(String(describing: res.result.error))")
            } else {
                print("API Response: (\(res.response!.statusCode)) [\(res.timeline.totalDuration)s] Response:\(String(describing: res.result.value))")
            }
        }
    }
    
    /// Uploading
    
    func upload(
        _ URLRequest: URLRequestConvertible,
        multipartFormData: (MultipartFormData) -> Void,
        encodingCompletion: ((SessionManager.MultipartFormDataEncodingResult) -> Void)?) -> Void
    {
        let formData = MultipartFormData()
        multipartFormData(formData)
        
        var URLRequestWithContentType = try? URLRequest.asURLRequest()
        
        URLRequestWithContentType?.setValue(formData.contentType, forHTTPHeaderField: "Content-Type")
        
        let fileManager = FileManager.default
        let tempDirectoryURL = URL(fileURLWithPath: NSTemporaryDirectory())
        let fileName = UUID().uuidString
        
        #if swift(>=2.3)
        let directoryURL = tempDirectoryURL.appendingPathComponent("com.alamofire.manager/multipart.form.data")
        let fileURL = directoryURL.appendingPathComponent(fileName)
        #else
        
        let directoryURL = tempDirectoryURL.appendingPathComponent("com.alamofire.manager/multipart.form.data")
        let fileURL = directoryURL.appendingPathComponent(fileName)
        #endif
        
        
        do {
            try fileManager.createDirectory(at: directoryURL, withIntermediateDirectories: true, attributes: nil)
            try formData.writeEncodedData(to: fileURL)
            
            DispatchQueue.main.async {
                
                let encodingResult = SessionManager.MultipartFormDataEncodingResult.success(request: SessionSharedManager.shared.upload(fileURL, with: URLRequestWithContentType!), streamingFromDisk: true, streamFileURL: fileURL)
                encodingCompletion?(encodingResult)
            }
        } catch {
            DispatchQueue.main.async {
                encodingCompletion?(.failure(error as NSError))
            }
        }
    }
    
    // HTTPs Methods
    func GET(param parameters:[String: AnyObject]?, getURl: String?, success:ClosureSuccess?,  failure:ClosureError?) -> URLSessionTask?
    {
        let uRequest = SessionSharedManager.shared.request(getURl!, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers)
        self.logging(request: uRequest)
        
        uRequest.responseJSON { (response) in
            
            self.logging(response: response)
            if(response.result.error == nil)
            {
                if(success != nil) {
                    success!(uRequest.task!, response.result.value as AnyObject)
                }
            }
            else
            {
                if(failure != nil) {
                    failure!(uRequest.task!, response.result.error as NSError?)
                }
            }
        }
        
        return uRequest.task
    }
    
    func GET(param parameters:[String: AnyObject]?, tag:String?, success:ClosureSuccess?,  failure:ClosureError?) -> URLSessionTask?
    {
        
        
        let uRequest = SessionSharedManager.shared.request((BASEURL+tag!), method: .get, parameters: parameters, encoding: JSONEncoding.prettyPrinted, headers: headers)
        self.logging(request: uRequest)
        
        
        uRequest.responseJSON { (response) in
            
            self.logging(response: response)
            if(response.result.error == nil)
            {
                if(success != nil) {
                    success!(uRequest.task!, response.result.value as AnyObject)
                }
            }
            else
            {
                if(failure != nil) {
                    failure!(uRequest.task!, response.result.error as NSError?)
                }
            }
        }
        
        return uRequest.task
    }
    
    func GET(param parameters:[String: AnyObject]?, success:ClosureSuccess?,  failure:ClosureError?) -> URLSessionTask?
    {
        
        let uRequest = SessionSharedManager.shared.request(BASEURL, method: .get, parameters: parameters, encoding: JSONEncoding.prettyPrinted, headers: headers)
        self.logging(request: uRequest)
        
        uRequest.responseJSON { (response) in
            
            self.logging(response: response)
            if(response.result.error == nil)
            {
                if(success != nil) {
                    success!(uRequest.task!, response.result.value as AnyObject)
                }
            }
            else
            {
                if(failure != nil) {
                    failure!(uRequest.task!, response.result.error as NSError?)
                }
            }
        }
        
        return uRequest.task
    }
    
    func POST(param parameters:[String: AnyObject]?, success:ClosureSuccess?,  failure:ClosureError?) -> URLSessionTask?
    {
        
        let uRequest = SessionSharedManager.shared.request((BASEURL + (parameters?["tag"] as? String ?? "")), method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        self.logging(request: uRequest)
        
        uRequest.responseJSON { (response) in
            
            self.logging(response: response)
            if(response.result.error == nil)
            {
                if(success != nil) {
                    success!(uRequest.task!, response.result.value as AnyObject)
                }
            }
            else
            {
                if(failure != nil) {
                    failure!(uRequest.task!, response.result.error as
                        NSError?)
                }
            }
        }
        
        return uRequest.task
    }
    func POSTWebHook(param parameters:[String: AnyObject]?, tag:String?, success:ClosureSuccess?,  failure:ClosureError?) -> URLSessionTask?
    {
        //        headers = ["Content-Type": "application/x-www-form-urlencoded"] //, "Accept": "application/json"
        print(GniBASEURL as Any)
        
        let uRequest = SessionSharedManager.shared.request((GniBASEURL + tag!), method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        self.logging(request: uRequest)
        
        uRequest.responseJSON { (response) in
            
            self.logging(response: response)
            if(response.result.error == nil)
            {
                if(success != nil) {
                    success!(uRequest.task!, response.result.value as AnyObject)
                }
            }
            else
            {
                if(failure != nil) {
                    //failure!(uRequest.task!, response.result.error as
                      //  NSError?)
                }
            }
        }
        
        return uRequest.task
    }
    func POST(param parameters:[String: AnyObject]?, tag:String?, success:ClosureSuccess?,  failure:ClosureError?) -> URLSessionTask?
    {
//        headers = ["Content-Type": "application/x-www-form-urlencoded"] //, "Accept": "application/json"
        print(BASEURL as Any)
        
        let uRequest = SessionSharedManager.shared.request((BASEURL + tag!), method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        self.logging(request: uRequest)
        
        uRequest.responseJSON { (response) in
            
            self.logging(response: response)
            if(response.result.error == nil)
            {
                if(success != nil) {
                    success!(uRequest.task!, response.result.value as AnyObject)
                }
            }
            else
            {
                if(failure != nil) {
                    failure!(uRequest.task!, response.result.error as
                        NSError?)
                }
            }
        }
        
        return uRequest.task
    }
    
    func notPrettyString(from object: Any) -> String? {

        if let objectData = try? JSONSerialization.data(withJSONObject: object, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            let objectString = String(data: objectData, encoding: .utf8)
            return objectString
        }
        return nil
    }
    
    func POST(param parameters:[String: AnyObject]?, tag:String?, multipartFormData: @escaping (MultipartFormData) -> Void, success:ClosureSuccess?,  failure:ClosureError?) -> Void
    {
        
        
        SessionSharedManager.shared.upload(multipartFormData: { (multipart) in
            multipartFormData(multipart)
            
            for (key, value) in parameters! {
                
                
                
                if let value = value as? String {
                    
                    multipart.append(value.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! , withName: key)
                    
                }
                else if let value = value as? Int {
                    let strValue = String(value)
                    multipart.append(strValue.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! , withName: key)
                }
                
                if let arryReq = value as? Array<[String: AnyObject]> {
                    let str = self.notPrettyString(from: arryReq)!
                    
                    multipart.append(str.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! , withName: key)
                }
            }
            
        },  to: (BASEURL + tag!), method: HTTPMethod.post , headers: headers) { (encodingResult) in
            
            switch encodingResult {
                
            case .success(let uRequest, _, _):
                
                self.logging(request: uRequest)
                
                uRequest.responseJSON { (response) in
                    
                    self.logging(response: response)
                    if(response.result.error == nil)
                    {
                        if(success != nil) {
                            success!(uRequest.task!, response.result.value as AnyObject)
                        }
                    }
                    else
                    {
                        if(failure != nil) {
                            failure!(uRequest.task!, response.result.error as NSError?)
                        }
                    }
                }
                
                break
            case .failure(let encodingError):
                print(encodingError)
                break
            }
        }
        
    }
}


//MARK:-
//MARK:- APIRequest Class

class APIRequest {
    
    
    typealias ClosureCompletion = (_ response:AnyObject?, _ error:NSError?) -> Void
    
    typealias successCallBack = (([String:AnyObject]?) -> ())
    typealias successCallBackArr = (([Any]?) -> ())
    typealias failureCallBack = ((String) -> ())
    
    //var BASEURL:String      =   "http://sandboxapi.medicalwale.com/v47/auth/" // Test URL
    var BASEURL:String = "https://39219992.ngrok.io/eshiksa/"
    var productOffset = 0;
    //MARK:- APIRequest Singleton
    
    static let shared = APIRequest()
    
    private init() {
    }
    
//    private var apiRequest:APIRequest {
//        let apiRequest = APIRequest()
//
//        Networking.sharedInstance.BASEURL = BASEURL
//        return apiRequest
//    }
//
//    static func shared() -> APIRequest {
//        return APIRequest().apiRequest
//    }
    
    private func handleStatusCode(response:AnyObject? ,suceessAlert:Bool, failurAlert:Bool, successCallBack:successCallBack , failureCallBack:failureCallBack) {
        
        if (response != nil && (response as? [String:Any]) != nil)
        {
            let dict =  response as? [String: Any]
            
            if (dict?[CJsonError] as! NSString) as String == CJsonErrorFalse
            {
                successCallBack(response as? [String : AnyObject] ?? nil)
                if(suceessAlert)
                {
                   // MIToastAlert.shared.showToastAlert(position: .bottom, message: dict?[CJsonMessage] as! String)
                }
                print("yes")
            }
            else
            {
                if dict?[CJsonStatus] as! NSNumber == CStatusFour
                {
                    
                    
                }
                else if (dict?[CJsonStatus] as! NSNumber == CStatusThree || dict?[CJsonStatus] as! NSNumber == CStatusTwo)
                {
                   // MIToastAlert.shared.showToastAlert(position: .bottom, message: dict?[CJsonMessage] as! String)
                }
                else{
                    if (failurAlert)
                    {
                       // MIToastAlert.shared.showToastAlert(position: .center, message: dict?[CJsonMessage] as! String)
                    }
                }
                
                failureCallBack(response?[CJsonMessage] as? String ?? "")
                
            }
        }
        else
        {
            if ((response?[CStatusCode] as? NSNumber) == CStatusFourHundredAndOne)
            {
              //  MIToastAlert.shared.showToastAlert(position: .center, message: response![CJsonMessage] as! String)
            }
            else if (response?[CStatusCode] as? NSNumber) == CStatusZero
            {
                failureCallBack(response?[CJsonMessage] as? String ?? "")
            }
            else if (failurAlert)
            {
               // MIToastAlert.shared.showToastAlert(position: .center, message: response![CJsonMessage] as! String)
            }
            else if ((response?[CStatusCode] as? NSNumber) == CStatusFiveHundredAndFiftyFive || (response?[CStatusCode] as? NSNumber) == CStatusFiveHundredAndFiftySix || (response?[CStatusCode] as? NSNumber) == CStatusFiveHundredAndFifty)
            {
              //  MIToastAlert.shared.showToastAlert(position: .center, message: response![CJsonMessage] as! String)
            }
            
        }
        
    }
    
    func isValidData(response:[String : AnyObject]?) -> Bool
    {
        guard response != nil else { return false }
        
        if let status = response![CJsonStatus] as? Int
        {
            if status != 200 { return false }
        }
        if let error = response!["error"] as? String
        {
            if error != "false" { return false }
        }
        if let data = response![CJsonData] as? Array<[String: AnyObject]>
        {
            return data.count > 0
        }
        if let data = response![CJsonData] as? [String: AnyObject]
        {
            return data.count > 0
        }
        
        return false
    }
    
    func isValidResponse(response:[String : AnyObject]?) -> Bool
    {
        guard response != nil else { return false }
        
        if let status = response![CJsonStatus] as? Int
        {
            if status != 200 { return false }
        }
        if let error = response!["error"] as? String
        {
            if error != "false" { return false }
        }
        return true
    }
    
   // MARK:- APIRequest Methods
    

  
        
        func callWebServiceWithMultiPartWithReturnDict(param:[String:AnyObject], apiTag:String?, successCallBack:@escaping successCallBack , failureCallBack:@escaping failureCallBack)
        {
        
            
               
            _ = Networking.sharedInstance.POST(param: param, tag: apiTag, multipartFormData: { (multipartFormData) in
                
            }, success: { (task, response) in
                 successCallBack(response  as? [String : AnyObject])
            }, failure: { (task, error) in
                failureCallBack((error?.localizedDescription)!)
            })
                
               // SKActivityIndicator.dismiss()
           
            
        }
    func callWebServiceWithMultiPartArray(param:[String:AnyObject], apiTag:String?, successCallBack:@escaping successCallBackArr , failureCallBack:@escaping failureCallBack)
    {
        
        
        
        _ = Networking.sharedInstance.POST(param: param, tag: apiTag, multipartFormData: { (multipartFormData) in
            
        }, success: { (task, response) in
            successCallBack(response  as? [Any])
        }, failure: { (task, error) in
            failureCallBack((error?.localizedDescription)!)
        })
        
        // SKActivityIndicator.dismiss()
        
        
    }
    
    func callWebServiceForJson(param:[String:AnyObject], apiTag:String?, successCallBack:@escaping successCallBackArr , failureCallBack:@escaping failureCallBack)
    {
        // SKActivityIndicator.show("Loading...")
        
        _ = Networking.sharedInstance.POSTWebHook(param: param, tag:apiTag  ,success: { (task, response) in
        
            successCallBack(response  as? [Any])
            
            
            // SKActivityIndicator.dismiss()
        } , failure: { (task, error) in
            // SKActivityIndicator.dismiss()
            failureCallBack((error?.localizedDescription)!)
        })
        
    }
    
    func handleAPIResponse(response:AnyObject?) -> Bool {
        
        if let dictResponse = response {
            
            var apiCode: Int = 0
            if let code = dictResponse[CJsonCode] as? NSNumber {
                apiCode = code.intValue
            }
            else if let code = dictResponse[CJsonCode] as? String {
                apiCode = Int(code)!
            }
            print(apiCode)
            
            if apiCode == 0 {
                
                return true
            } else if apiCode == 500 {
                
                return false
            } else {
                
                if apiCode == 111 {
                    return true
                }
                // MIToastAlert.shared.showToastAlert(position: .bottom, message: dictResponse[CJsonMessage] as! String)
                return false
            }
        }
        return false
    }

}
class SessionSharedManager {
    
    static let shared = SessionManager()
    
    private init() {}
}




