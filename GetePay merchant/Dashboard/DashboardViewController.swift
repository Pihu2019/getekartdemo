//
//  DashboardViewController.swift
//  GetePay merchant
//
//  Created by shayam on 10/07/19.
//  Copyright © 2019 eshiksa. All rights reserved


import UIKit

class DashboardViewController: UIViewController {

    @IBOutlet weak var linkView: UIView!
    @IBOutlet weak var ideaView: UIView!
    @IBOutlet weak var bankingView: UIView!
    @IBOutlet weak var addView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    linkView.layer.cornerRadius = 10
    ideaView.layer.cornerRadius = 10
    bankingView.layer.cornerRadius = 10
    addView.layer.cornerRadius = 10
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
