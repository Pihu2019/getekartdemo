//
//  SubscriptionListVC.swift
//  GetePay merchant
//
//  Created by shayam on 20/07/19.
//  Copyright © 2019 eshiksa. All rights reserved.
//

import UIKit

class SubscriptionListVC: UIViewController {
    @IBOutlet weak var subscriptionTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



/*
 
 
 http://35.200.153.165:8080/getekart/Subscription/getSubscriptionlist
 
 {
 "mid":"6irdwDznArIvUbS3POLsqg==",
 "currentUser":"Help For Humanity",
 "signature":"69232962aef4388ad651734ad09edfe39378a1c59e14c37b3f024d0152fcb9bfce5b9cf274b0981af3725f70be6d7f87779169ef22ab08e06c77b5f88eeb4d74"
 }
 
 
 
 {
 "subscriptionList": [
 {
 "id": 1,
 "mid": 1,
 "subscriptionpackName": "Monthly",
 "subscriptionpackageAmount": 100,
 "subscriptionpackageValidity": 28,
 "createdDate": {
 "year": 2019,
 "month": "JULY",
 "monthValue": 7,
 "dayOfMonth": 19,
 "hour": 12,
 "minute": 29,
 "second": 32,
 "nano": 835000000,
 "dayOfWeek": "FRIDAY",
 "dayOfYear": 200,
 "chronology": {
 "id": "ISO",
 "calendarType": "iso8601"
 }
 },
 "modifiedDate": {
 "year": 2019,
 "month": "JULY",
 "monthValue": 7,
 "dayOfMonth": 19,
 "hour": 12,
 "minute": 29,
 "second": 32,
 "nano": 835000000,
 "dayOfWeek": "FRIDAY",
 "dayOfYear": 200,
 "chronology": {
 "id": "ISO",
 "calendarType": "iso8601"
 }
 },
 "createdBy": "Test_Merchant",
 "modifiedBy": "Test_Merchant",
 "isDefault": true,
 "colorCode": "Purple",
 "icon": "icon8-subscription2.png",
 "detail1": "detail-1",
 "detail2": "detail-2",
 "detail3": "detail-3",
 "detail4": "detail-4",
 "detail5": "detail-5",
 "enabled": true,
 "discount": 5,
 "discountBeforeValidity": 25,
 "token": "e17b14bb-0d16-44bb-96b6-e5305b29b54d",
 "expireTime": 1595183400000,
 "expired": false
 },
 {
 "id": 2,
 "mid": 1,
 "subscriptionpackName": "Quarterly",
 "subscriptionpackageAmount": 70,
 "subscriptionpackageValidity": 15,
 "createdDate": {
 "year": 2019,
 "month": "JULY",
 "monthValue": 7,
 "dayOfMonth": 19,
 "hour": 12,
 "minute": 32,
 "second": 10,
 "nano": 336000000,
 "dayOfWeek": "FRIDAY",
 "dayOfYear": 200,
 "chronology": {
 "id": "ISO",
 "calendarType": "iso8601"
 }
 },
 "modifiedDate": {
 "year": 2019,
 "month": "JULY",
 "monthValue": 7,
 "dayOfMonth": 19,
 "hour": 12,
 "minute": 32,
 "second": 10,
 "nano": 336000000,
 "dayOfWeek": "FRIDAY",
 "dayOfYear": 200,
 "chronology": {
 "id": "ISO",
 "calendarType": "iso8601"
 }
 },
 "createdBy": "Test_Merchant",
 "modifiedBy": "Test_Merchant",
 "isDefault": true,
 "colorCode": "DeepSkyBlue",
 "icon": "icon8-subscription5.png",
 "detail1": "detail-1",
 "detail2": "detail-2",
 "detail3": "detail-3",
 "detail4": "detail-4",
 "detail5": "detail-5",
 "enabled": true,
 "discount": 10,
 "discountBeforeValidity": 14,
 "token": "978ee07d-a549-4251-b558-5b83994a73c8",
 "expireTime": 1595183400000,
 "expired": false
 },
 {
 "id": 3,
 "mid": 1,
 "subscriptionpackName": "Yearly",
 "subscriptionpackageAmount": 1200,
 "subscriptionpackageValidity": 365,
 "createdDate": {
 "year": 2019,
 "month": "JULY",
 "monthValue": 7,
 "dayOfMonth": 19,
 "hour": 12,
 "minute": 33,
 "second": 45,
 "nano": 661000000,
 "dayOfWeek": "FRIDAY",
 "dayOfYear": 200,
 "chronology": {
 "id": "ISO",
 "calendarType": "iso8601"
 }
 },
 "modifiedDate": {
 "year": 2019,
 "month": "JULY",
 "monthValue": 7,
 "dayOfMonth": 19,
 "hour": 12,
 "minute": 33,
 "second": 45,
 "nano": 661000000,
 "dayOfWeek": "FRIDAY",
 "dayOfYear": 200,
 "chronology": {
 "id": "ISO",
 "calendarType": "iso8601"
 }
 },
 "createdBy": "Test_Merchant",
 "modifiedBy": "Test_Merchant",
 "isDefault": false,
 "colorCode": "DarkSlateGray",
 "icon": "icon8-subscription5.png",
 "detail1": "detail-1",
 "detail2": "detail-2",
 "detail3": "detail-3",
 "detail4": "detail-4",
 "detail5": "detail-5",
 "enabled": true,
 "discount": 100,
 "discountBeforeValidity": 300,
 "token": "54832f6c-e567-4b31-a636-d18f022996da",
 "expireTime": 1595183400000,
 "expired": false
 },
 {
 "id": 4,
 "mid": 1,
 "subscriptionpackName": "saddas",
 "subscriptionpackageAmount": 400,
 "subscriptionpackageValidity": 30,
 "createdDate": {
 "year": 2019,
 "month": "JULY",
 "monthValue": 7,
 "dayOfMonth": 19,
 "hour": 16,
 "minute": 59,
 "second": 56,
 "nano": 289000000,
 "dayOfWeek": "FRIDAY",
 "dayOfYear": 200,
 "chronology": {
 "id": "ISO",
 "calendarType": "iso8601"
 }
 },
 "modifiedDate": {
 "year": 2019,
 "month": "JULY",
 "monthValue": 7,
 "dayOfMonth": 19,
 "hour": 16,
 "minute": 59,
 "second": 56,
 "nano": 289000000,
 "dayOfWeek": "FRIDAY",
 "dayOfYear": 200,
 "chronology": {
 "id": "ISO",
 "calendarType": "iso8601"
 }
 },
 "createdBy": "Test_Merchant",
 "modifiedBy": "Test_Merchant",
 "isDefault": true,
 "colorCode": "green",
 "icon": "icon8-subscription1.png",
 "detail1": "abc",
 "detail2": "abc",
 "detail3": "abc",
 "detail4": "abc",
 "detail5": "abc",
 "enabled": true,
 "discount": 100,
 "discountBeforeValidity": 25,
 "token": "9facdb09-2d9b-4024-887d-25ab18c264bf",
 "expireTime": 1559500200000,
 "expired": true
 }
 ],
 "status": 200,
 "message": null
 }
 
 
 */
