//
//  FeedbackListViewController.swift
//  GetePay merchant
//
//  Created by shayam on 20/07/19.
//  Copyright © 2019 eshiksa. All rights reserved.
//

import UIKit

class FeedbackListViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

/*
 
 
 URL :  http://35.200.153.165:8080/getekart/header/getFeedbackDetail
 
 
 Request:-
 
 {
 "mid" : "6irdwDznArIvUbS3POLsqg==",
 "signature" : "8415d78532fb8da716dc2306580915d44fd4293a3a6d0512fe9863955ecb74dfefb45f5e6f720554e1f57ba476501df6a00d78c50692814538a8812bb7ec5b10"
 }
 
 
 
 Response:-
 
 {
 "mId": 1,
 "feedbackList": [
 {
 "id": 18,
 "mId": 1,
 "firstName": "This",
 "lastName": "is ",
 "email": "ojha92@gmail.com",
 "mobileNo": "1111111111",
 "remark": "This is test",
 "createdDate": {
 "monthValue": 7,
 "year": 2019,
 "month": "JULY",
 "dayOfMonth": 15,
 "dayOfWeek": "MONDAY",
 "dayOfYear": 196,
 "hour": 10,
 "minute": 2,
 "second": 25,
 "nano": 40000000,
 "chronology": {
 "calendarType": "iso8601",
 "id": "ISO"
 }
 },
 "modifyDate": {
 "monthValue": 7,
 "year": 2019,
 "month": "JULY",
 "dayOfMonth": 15,
 "dayOfWeek": "MONDAY",
 "dayOfYear": 196,
 "hour": 10,
 "minute": 2,
 "second": 25,
 "nano": 41000000,
 "chronology": {
 "calendarType": "iso8601",
 "id": "ISO"
 }
 },
 "createdBy": "This",
 "modifyBy": "This",
 "status": true
 },
 {
 "id": 19,
 "mId": 1,
 "firstName": "dsfdsf",
 "lastName": "sdfsdf",
 "email": "dsf@d",
 "mobileNo": "234234",
 "remark": "dsfsdf",
 "createdDate": {
 "monthValue": 7,
 "year": 2019,
 "month": "JULY",
 "dayOfMonth": 15,
 "dayOfWeek": "MONDAY",
 "dayOfYear": 196,
 "hour": 10,
 "minute": 19,
 "second": 57,
 "nano": 892000000,
 "chronology": {
 "calendarType": "iso8601",
 "id": "ISO"
 }
 },
 "modifyDate": {
 "monthValue": 7,
 "year": 2019,
 "month": "JULY",
 "dayOfMonth": 15,
 "dayOfWeek": "MONDAY",
 "dayOfYear": 196,
 "hour": 10,
 "minute": 19,
 "second": 57,
 "nano": 893000000,
 "chronology": {
 "calendarType": "iso8601",
 "id": "ISO"
 }
 },
 "createdBy": "dsfdsf",
 "modifyBy": "dsfdsf",
 "status": true
 }
 ]
 }
 

 
 */


