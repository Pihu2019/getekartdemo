//
//  LoginViewController.swift
//  GetePay merchant
//
//  Created by shayam on 10/07/19.
//  Copyright © 2019 eshiksa. All rights reserved.
//

import UIKit
import Foundation

class LoginViewController: UIViewController {
    
@IBOutlet weak var username: UITextField!
@IBOutlet weak var password: UITextField!
@IBOutlet weak var imgView: UIImageView!
    
@IBOutlet weak var loginBtn: UIButton!
@IBOutlet weak var createBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loginBtn.layer.cornerRadius = 10
        imgView.layer.cornerRadius = 10
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func loginAPI(){
        
        
        let headers = [
            "Content-Type": "application/json",
            "cache-control": "no-cache",
            "Postman-Token": "05181b3b-1be5-4ade-8b4c-0539ca8d5e2d"
        ]
        let parameters = [
            "mId": "6irdwDznArIvUbS3POLsqg==",
            "password": "Admin@123",
            "username": "priyagongal",
            "signature": "66ca02ed9a7783fe2dd6bdbd420c2cca96954a7587ac8c8e04e254639d43b8ef99d6b7d6f98baf7ba7a4f5e9fca69f3458bfa68eefa6da060a52418ce4d02860"
            ] as [String : Any]
        
       // let postData = JSONSerialization.data(withJSONObject: parameters, options: [])
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://35.200.153.165:8080/getekart/merchantlogin/appMerchantLogin")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
       // request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse)
            }
        })
        
        dataTask.resume()
    }

}

/*
 http://35.200.153.165:8080/getekart/merchantlogin/appMerchantLogin

 {
 "mId":"6irdwDznArIvUbS3POLsqg==",
 "password":"Admin@123",
 "username":"priyagongal",
 "signature":"66ca02ed9a7783fe2dd6bdbd420c2cca96954a7587ac8c8e04e254639d43b8ef99d6b7d6f98baf7ba7a4f5e9fca69f3458bfa68eefa6da060a52418ce4d02860"
 }
 
 
 
 {
 "username": "priyagongal",
 "permission": [],
 "status": 200,
 "message": null
 }
 
 */
