//
//  AppDelegate.swift
//  EshiksaNewApp
//
//  Created by shayam on 27/06/19.
//  Copyright © 2019 eshiksa. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    // var window: UIWindow?
    let window = UIWindow(frame: UIScreen.main.bounds)
    var rootVCNav: UINavigationController!
    
    var reachability: Reachability?
    let hostNames = [nil, "google.com", "invalidhost"]
    var hostIndex = 0
    var checknoconnectionTime = 0
    var checkConnection = 0 as Int
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        initiateApplicationRoot(launchOptions: launchOptions)
        
        
        return true
    }
    
    func initiateApplicationRoot(launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        var rootVC: UIViewController!
        
        if  ((Datacache.getValueFromUserDefault(forKey: CUserData)) == nil || (Datacache.getValueFromUserDefault(forKey: CUserData))?.count == 0)
        {
            guard let vcVerification = CMain.instantiateViewController(withIdentifier: "LoginViewControllerID") as? LoginViewController else {return}
            //  vcVerification.isFromLogin = false
            rootVC = UINavigationController(rootViewController: vcVerification)
        }
        else{
            
        }
        //
        self.setWindowRootViewController(rootVC: rootVC, animated: false, completion: nil)
        self.window.makeKeyAndVisible()
        
    }
    func setWindowRootViewController(rootVC:UIViewController?, animated:Bool, completion: ((Bool) -> Void)?) {
        
        guard rootVC != nil else {
            return
        }
        
        UIView.transition(with: self.window, duration: animated ? 0.6 : 0.0, options: .transitionCrossDissolve, animations: {
            
            let oldState = UIView.areAnimationsEnabled
            UIView.setAnimationsEnabled(false)
            
            self.window.rootViewController = rootVC
            UIView.setAnimationsEnabled(oldState)
        }) { (finished) in
            if let handler = completion {
                handler(true)
                
            }
        }
    }

    //MARK:- NetworkConnection
    
    func startHost(at index: Int) {
        stopNotifier()
        setupReachability(hostNames[index], useClosures: true)
        startNotifier()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.startHost(at: (index + 1) % 3)
        }
    }
    
    func setupReachability(_ hostName: String?, useClosures: Bool) {
        let reachability: Reachability?
        if let hostName = hostName {
            reachability = Reachability(hostname: hostName)
            // hostNameLabel.text = hostName
        } else {
            reachability = Reachability()
            // hostNameLabel.text = "No host name"
        }
        self.reachability = reachability
        // print("--- set up with host name: \(hostNameLabel.text!)")
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(reachabilityChanged(_:)),
            name: .reachabilityChanged,
            object: reachability
        )
    }
    
    func startNotifier() {
        print("--- start notifier")
        do {
            try reachability?.startNotifier()
        } catch {
            // networkStatus.textColor = .red
            self.checkConnection = 0
            // networkStatus.text = "Unable to start\nnotifier"
            return
        }
    }
    
    func stopNotifier() {
        print("--- stop notifier")
        reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: nil)
        reachability = nil
    }
    
    func updateLabelColourWhenReachable(_ reachability: Reachability) {
        print("\(reachability.description) - \(reachability.connection)")
        if reachability.connection == .wifi {
            //  self.networkStatus.textColor = .green
            self.checkConnection = 1
        } else {
            
            self.checkConnection = 1
            //self.networkStatus.textColor = .blue
        }
        
        // self.networkStatus.text = "\(reachability.connection)"
    }
    
    func updateLabelColourWhenNotReachable(_ reachability: Reachability) {
        print("\(reachability.description) - \(reachability.connection)")
        
        
        // self.networkStatus.textColor = .red
        self.checkConnection = 0
        // self.networkStatus.text = "\(reachability.connection)"
    }
    
    @objc func reachabilityChanged(_ note: Notification) {
        let reachability = note.object as! Reachability
        
        
        if reachability.connection != .none {
            checknoconnectionTime = 0
            updateLabelColourWhenReachable(reachability)
        } else {
            if checknoconnectionTime  > 1
            {
                updateLabelColourWhenNotReachable(reachability)
                
            }
            else
            {
                checknoconnectionTime = checknoconnectionTime + 1
            }
            
        }
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}

